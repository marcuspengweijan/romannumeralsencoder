﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using RomanNumeralsEncoder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RomanNumeralsEncoder.Tests
{
    [TestClass]
    public class KataTests
    {
        [DataRow(1, "I", DisplayName = "Input: 1, Expect: I")]
        [DataRow(2, "II", DisplayName = "Input: 2, Expect: II")]
        [DataRow(4, "IV", DisplayName = "Input: 4, Expect: IV")]
        [DataRow(500, "D", DisplayName = "Input: 500, Expect: D")]
        [DataRow(1000, "M", DisplayName = "Input: 1000, Expect: M")]
        [DataRow(1954, "MCMLIV", DisplayName = "Input: 1954, Expect: MCMLIV")]
        [DataRow(1990, "MCMXC", DisplayName = "Input: 1990, Expect: MCMXC")]
        [DataRow(2008, "MMVIII", DisplayName = "Input: 2008, Expect: MMVIII")]
        [DataRow(2014, "MMXIV", DisplayName = "Input: 2014, Expect: MMXIV")]
        [DataRow(3999, "MMMCMXCIX", DisplayName = "Input: 3999, Expect: MMMCMXCIX")]
        [DataTestMethod]
        public void SolutionTest(int input, string expected)
        {
            Assert.AreEqual(expected, Kata.Solution(input));
        }
    }
}