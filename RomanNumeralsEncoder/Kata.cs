﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace RomanNumeralsEncoder
{
    public class Kata
    {
        private static readonly Dictionary<string, int> _RomanSymbol = new Dictionary<string, int>
        {
            { "I", 1 },
            { "V", 5 },
            { "X", 10 },
            { "L", 50},
            { "C", 100 },
            { "D", 500 },
            { "M", 1000 },

            { "IV", 4 },
            { "IX", 9 },
            { "XL", 40 },
            { "XC", 90},
            { "CD", 400},
            { "CM", 900}
        };

        public static string Solution(int n)
        {
            string result = string.Empty;

            foreach (KeyValuePair<string, int> romanSymbol in _RomanSymbol.OrderByDescending(x => x.Value))
            {
                result += string.Concat(Enumerable.Repeat(romanSymbol.Key, n / romanSymbol.Value));
                n %= romanSymbol.Value;
            }

            return result;
        }
    }
}
